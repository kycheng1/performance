/*global __ENV : true  */
/*
@endpoint: `GET /projects/:id/repository/commits?all=true`
@example_uri: /api/v4/projects/:encoded_path/repository/commits?all=true
@description: [Get a list of all repository commits in a project](https://docs.gitlab.com/ee/api/commits.html#list-repository-commits)
@issue: https://gitlab.com/gitlab-org/gitlab/-/issues/443673
@gpt_data_version: 1
*/

import http from "k6/http";
import { group } from "k6";
import { Rate } from "k6/metrics";
import { logError, getRpsThresholds, getTtfbThreshold, getLargeProjects, selectRandom } from "../../lib/gpt_k6_modules.js";

export let rpsThresholds = getRpsThresholds()
export let ttfbThreshold = getTtfbThreshold()
export let successRate = new Rate("successful_requests")
export let options = {
  thresholds: {
    "successful_requests": [`rate>${__ENV.SUCCESS_RATE_THRESHOLD}`],
    "http_req_waiting": [`p(90)<${ttfbThreshold}`],
    "http_reqs": [`count>=${rpsThresholds['count']}`]
  },
  scenarios: {
    gpt: {
      executor: 'ramping-arrival-rate',
      preAllocatedVUs: __ENV.OPTION_RPS,
      maxVUs: __ENV.OPTION_MAX_VUS,
      stages: JSON.parse(__ENV.OPTION_STAGES),
      gracefulStop: '1800s',
    },
  },
  discardResponseBodies: true // Configure k6 not to load response body with repository to memory
};

export let projects = getLargeProjects(['encoded_path']);

export function setup() {
  console.log('')
  console.log(`RPS Threshold: ${rpsThresholds['mean']}/s (${rpsThresholds['count']})`)
  console.log(`TTFB P90 Threshold: ${ttfbThreshold}ms`)
  console.log(`Success Rate Threshold: ${parseFloat(__ENV.SUCCESS_RATE_THRESHOLD)*100}%`)
}
export default function() {
  group("API - Project Repository Commits List All", function() {
    let project = selectRandom(projects);

    let params = { headers: { "Accept": "application/json", "PRIVATE-TOKEN": `${__ENV.ACCESS_TOKEN}` } };
    let res = http.get(`${__ENV.ENVIRONMENT_URL}/api/v4/projects/${project['encoded_path']}/repository/commits?all=true`, params);
    /20(0|1)/.test(res.status) ? successRate.add(true) : (successRate.add(false), logError(res));
  });
}
